var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var logger = require('morgan');
const bodyParser = require("body-parser");
const exphbs=require('express-handlebars');
var cookieParser = require('cookie-parser');
var fileUpload = require('express-fileupload');


var regRouter = require('./routes/register');
var passwordResetRouter = require('./routes/password_reset');
var indexRouter = require('./routes/index').router;
var loginRouter = require('./routes/login');
var izlozakRouter = require('./routes/exhibits'); //promijenjeno ime samo
var usersRouter = require('./routes/users'); 
var aboutRouter = require('./routes/about_us').router;
var promoRouter = require('./routes/promo');
var controlRouter = require('./routes/control_panel');
var exhibitPageRouter = require('./routes/exhibitpage');
var confirmationRouter = require('./routes/confirmation');
var messageRouter = require('./routes/message');
var downloadRouter= require('./routes/download');
var profileRouter = require('./routes/profile');
var playsRouter = require('./routes/plays');

var app = express();


// view engine setup
app.engine('handlebars',exphbs({ defaultLayout:'main' }));
app.set('view engine', 'handlebars');
app.set(express.static(path.join(__dirname,'public')));
app.use(cookieParser());

app.use(logger('dev'));
app.use(session({
  secret: 'w4e87yt9novb42537ty8nhv43vtn7y8', // vrlo random
  resave: true,
  saveUninitialized: true
}));

app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/register', regRouter);
app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/reset-password', passwordResetRouter);
app.use('/exhibitions/id', izlozakRouter);
app.use('/users', usersRouter);
app.use('/about_us', aboutRouter);
app.use('/promo', promoRouter);
app.use('/control_panel', controlRouter);
app.use('/exhibit/id', exhibitPageRouter);
app.use('/confirmation', confirmationRouter);
app.use('/message', messageRouter);
app.use('/download', downloadRouter);
app.use('/profile' , profileRouter);
app.use('/plays', playsRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;