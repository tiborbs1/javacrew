const checkCookie = (req,res,next)=>{
    if(typeof req.cookies.aviomuzejUserName !== 'undefined' && req.cookies.aviomuzejUserName !== null && typeof req.cookies.aviomuzejUserName.username !== 'undefined'){
      req.session.username = req.cookies.aviomuzejUserName.username;
      req.session.isAdmin = req.cookies.aviomuzejUserName.isAdmin;
      req.session.isOwner = req.cookies.aviomuzejUserName.isOwner;
      req.session.loggedin = true;
    }else{
      req.session.loggedin=false;
    }
    next();
}

module.exports=checkCookie;