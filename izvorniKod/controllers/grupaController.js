const Grupa= require('../models/Grupa');
var getAllExhibitionPromise = Grupa.getAllExhibition;
const Izlozak = require('../models/Izlozak').Izlozak;

const deleteGrupa = (req, res) => {
    // promijeni nacin na koji kupis ime grupe
    const sifraGrupePromise = Grupa.getGrupaByNaziv(req.body.naziv_grupa);
    // nadi sifru grupe pa izbrisi izloske i izloske data i onda izbrisi grupu -- nema krsenja FK
    sifraGrupePromise.then(group => {
         Izlozak.destroy({
            where: {
                sifra_grupa: group[0].sifra_grupa
            }
        }).then(() => {
            Grupa.removeGroupById(group[0].sifra_grupa);
            res.redirect("/control_panel");
        }).catch(e => console.log(e));
    });
};

module.exports = {
    getAllExhibitionPromise,
    deleteGrupa
}