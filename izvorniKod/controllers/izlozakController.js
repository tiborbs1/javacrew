const Izlozak = require('../models/Izlozak');
const Grupa = require('../models/Grupa');

/* za sad vraća sve izloške u muzeju, ali na kraju želimo da vraća izloške samo za određenu grupu*/
const getAllExhibits = (req,res) => {
  const getAllExhibitsPromise = Izlozak.getAllExhibits();
  const getAllExhibitionsPromise = Grupa.getAllExhibition();
  Promise.all([getAllExhibitsPromise, getAllExhibitionsPromise]).then((rezultati)=>{
    res.render('../views/grupe_izlozaka', {
      izlozak:rezultati[0], 
      grupa:rezultati[1],
      session_isAdmin:req.session.isAdmin,
      session_active:req.session.loggedin,
      session_isOwner:req.session.isOwner
    });
  }).catch((err) => {
    res.send(err);
  });
}

/*prostor za napravit funkciju koja vraća izloške po grupama*/
const getAllExhibitsByGroupId=(req, res) => {
  console.log('Request params: ');
    console.log(req.params.id);
    const getAllExhibitsByGroupIdPromise = Izlozak.getExhibitByGroupID(req.params.id);
    console.log(getAllExhibitsByGroupIdPromise)
    const getAllExhibitionsPromise = Grupa.getAllExhibition();
    Promise.all([getAllExhibitsByGroupIdPromise, getAllExhibitionsPromise]).then((rezultati)=>{
    
    rezultati[0].forEach(element => {
      console.log(element);
    });

    
    res.render('../views/grupe_izlozaka', {
      grupaIzlozaka:rezultati[0], 
      grupa:rezultati[1],
      session_isAdmin:req.session.isAdmin,
      session_active:req.session.loggedin,
      session_isOwner:req.session.isOwner
    });

  }).catch((err) => {
    res.send(err);
  });
}

const getSingleExhibit = (req, res) => {
  console.log('Request params: ');
  console.log(req.params.id);
  const getSingleExhibitPromise = Izlozak.getExhibitByID(req.params.id);
  console.log(getSingleExhibitPromise);
  getSingleExhibitPromise.then((izlozak) => {
    console.log(izlozak);
    if (!izlozak) {
      console.log("Nema ga");
    } else {
    const exhibit = {
      naziv_avion: izlozak.get('naziv_avion'),
      najveca_brzina: izlozak.get('najveca_brzina'),
      raspon_krila: izlozak.get('raspon_krila'),
      godina_proizvodnje: izlozak.get('godina_proizvodnje'),
      broj_izgradenih: izlozak.get('broj_izgradenih')
    }
    console.log(exhibit);
    res.render('exhibitpage', { 
      exhibit,
      session_isAdmin:req.session.isAdmin,
      session_active:req.session.loggedin,
      session_isAdmin:req.session.isOwner
    });
  }
  }).catch(err=>res.send(err));
}

const deleteIzlozak = (req, res) => {
  Izlozak.removeExhibitById(req.body.sifra_izlozak);
  res.redirect('/control_panel');

}

module.exports = {
    getAllExhibits,
    getAllExhibitsByGroupId,
    getSingleExhibit,
    deleteIzlozak
}