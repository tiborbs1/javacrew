const IzlozakData = require('../models/IzlozakData');
const Grupa = require('../models/Grupa');
const Visits = require('../models/Visits');
var moment = require('moment');

const getSingleExhibitData = (req, res) => {
  const getSingleExhibitDataPromise = IzlozakData.getExhibitDataByID(req.params.id);
  const getAllExhibitionsPromise = Grupa.getAllExhibition();
  const increaseVisitsPromise = Visits.increaseVisits(req.params.id);
  Promise.all([getSingleExhibitDataPromise, getAllExhibitionsPromise, increaseVisitsPromise]).then((rezultati)=>{
    res.render('../views/exhibitpage', {
      exhibit:rezultati[0], 
      grupa:rezultati[1],
      session_isAdmin:req.session.isAdmin,
      session_active:req.session.loggedin,
      session_isOwner: req.session.isOwner
    });
  }).catch((err) => {
    res.send(err);
  });  
}


const createIzlozakData = (req, res) => {

  //promijeni nacin na koji kupis naziv grupe
  const newIzlozakData = ({
  audio_pointer:  '/audio/' + req.files.audio.name,
  slika_pointer: '/images/' + req.files.image.name,
  naziv_grupa: req.body.browser,
  izlozak_opis: req.body.OpisIzloska,
  naziv_proizvodac: req.body.proizvodac,
  naziv_avion: req.body.naziv,
  najveca_brzina: req.body.najveca_brzina,
  raspon_krila: req.body.raspon_krila,
  godina_proizvodnje: req.body.godina_proizvodnje,
  broj_izgradenih: req.body.broj_izgradenih,
  naziv_motor: req.body.motor
});
  //formValidation(newIzlozakData, req, res)
  IzlozakData.createIzlozak(newIzlozakData);
}
 
  module.exports = {
      getSingleExhibitData,
      createIzlozakData
  }