const Promo = require('../models/Promo');
const Grupa = require('../models/Grupa');
let promoVisits = 0;

const getAllPromos = (req, res) => {
    promoVisits = promoVisits + 1;
    const getAllPromosPromise = Promo.getAllPromos();
    const getAllExhibitionsPromise = Grupa.getAllExhibition();
    Promise.all([getAllPromosPromise, getAllExhibitionsPromise]).then((rezultati)=>{
        res.render('../views/promo', {
          promo:rezultati[0], 
          grupa:rezultati[1],
          session_isAdmin:req.session.isAdmin,
          session_active:req.session.loggedin,
          session_isOwner:req.session.isOwner
        });
      }).catch((err) => res.send(err));  
}

module.exports = {
    getAllPromos,
    promoVisits
}