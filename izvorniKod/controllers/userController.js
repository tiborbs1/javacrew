const User = require('../models/User');
const UserInstance = require('../models/User').User;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');
const uuidv4 = require("uuid/v4");
const moment = require("moment");
const ForgotPasswordToken = require('../models/ForgotPasswordToken');

const EMAIL_SECRET = 'asdf1093KMnzxcvnkljvasdu09123nlasdasdf';
const SERVER_URL = 'https://134.209.233.66'

let transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'aviomuzej@gmail.com',
    pass: 'javacrewFER135'
  },
  tls: {
    rejectUnauthorized: false
  }
});

const getAllUsers = (req, res) => {

  const allUsersPromise = User.getAllUsers();

  return allUsersPromise.then(user => res.send(user))
    .catch(err => res.send(err));
}

const getUserByID = (req, res) => {

  const userByIDPromise = User.getUserByID(req.params.id)

  userByIDPromise.then((user) => {
    if (!user) {
      return res.status(404).send({
        message: 'User Not Found',
      });
    } else {
      korIme = user.korisnicko_ime
      console.log(kor)
    }

    return res.status(200).send(user);
  })
    .catch((error) => res.status(400).send(error));
}


const getUserByEmail = (req, res) => {

  const userByEmailPromise = User.findUserByEmail(req.body.email)

  userByEmailPromise.then((user) => {
    if (!user) {
      return res.status(404).send({
        message: 'User Not Found',
      });
    }
    return res.status(200).send(user);
  })
    .catch((error) => res.status(400).send(error));
}

const authUser = (request, response) => {
  const username = request.body.username.toString().trim();
  const password = request.body.password.toString();
  if (username && password) {

    const registeredUserbyUserNamePromise = User.findRegisteredUserByUserName(username);

    registeredUserbyUserNamePromise.then(user => {
      if (!user) {
        response.render('../views/message', {
          big_message: "Neuspješna prijava!",
          small_message: "Korisnik s navedenim korisničkim imenom ne postoji!",
          register_failed: false,
          register_sucess: false,
          login_failed: true
        });
      } else {
        if (bcrypt.compareSync(password, user.dataValues.lozinka)) {
          const isAdminPromise = User.isAdmin(username);

          const isOwnerPromise = User.isOwner(username);

          Promise.all([isAdminPromise, isOwnerPromise]).then((results) => {
            let isAdmin = results[0];
            let isOwner = results[1];
            if (isOwner) isAdmin = isOwner;
            request.session.username = username;
            request.session.isAdmin = isAdmin;
            request.session.isOwner = isOwner;
            request.session.loggedin = true;
            let userInfo = {
              username:username,
              isAdmin:isAdmin,
              isOwner:isOwner
            }
            User.loginUser(username).then(user => {
              console.log(username + ' logged in!')
            }).catch(err => console.log(err));
            response.cookie('aviomuzejUserName', userInfo, {maxAge: 52*604800000}).redirect('/');
          }).catch(err => console.log(err));
        } else {
          response.render('../views/message', {
            big_message: "Neuspješna prijava!",
            small_message: "Lozinka je netočna!",
            register_failed: false,
            register_sucess: false,
            login_failed: true
          });
        }
      }
    }).catch(err => {
      console.log(err);
    });
  } else {
    res.render('../views/message', {
      big_message: "Greška!",
      small_message: "Došlo je do greške, molimo Vas da unesete svoje korisničko ime i lozinku",
      register_failed: false,
      register_sucess: false
    });
  }
};

/* logout user */
const logOutUser = (request, response) => {
  User.logOutUser(request.session.username).then(user => {
    console.log(request.session.username + ' logged out!')
  }).catch(err => console.log(err));
  request.session.destroy();
  if(request.cookies.aviomuzejUserName) response.clearCookie('aviomuzejUserName', {maxAge: 52*604800000, path: '/'});
  response.redirect('/');
};

/* print out currently active user */
const debugUser = (request, response) => {
  response.send("loggedin=" + request.session.loggedin + " username=" + request.session.username + " isAdmin=" + request.session.isAdmin + " isOwner=" + request.session.isOwner);
  response.end();
};

const sendResetPasswordEmail = (req, res) => {
  // noinspection DuplicatedCode
  const email_address = req.body.email;
  console.log(req.body)
  if (email_address) {
    const userByEmailPromise = User.findUserByEmail(email_address);

    userByEmailPromise.then((user) => {
      if (user) {


        let token = uuidv4();
        let oneDayFromToday = moment().add(1, "days").toDate();

        const createNewTokenPromise = ForgotPasswordToken.createNewToken(user.dataValues.sifra_osoba, token, oneDayFromToday);


        createNewTokenPromise.then(function (result) {

          const url = SERVER_URL + '/users/resetPassword?token=' + token

          let mailOptions = {
            from: '"AvioMuzej Password Reset" <aviomuzej@gmail.com>',
            to: user.dataValues.email,
            subject: 'Reset password',
            html: `Please click this email to reset your password: <a href="${url}">${url}</a>`
          }

          transporter.sendMail(mailOptions, (error, info) => {
              if (error) {
                console.log(error);
              } else {
                console.log('Message sent: %s', info.messageId);
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
              }
            }
          );
          console.log(result);
        }).catch(err => {
          console.log(err);
        });
      }
      res.render('../views/message', {
        big_message: "Uspjeh!",
        small_message: "Email za promjenu korisničke lozinke je uspješno poslan ako Vaš email postoji u našem sistemu.",
        register_failed: false,
        register_sucess: false
      });
    }).catch((error) => res.status(400).send(error));
  } else {
    res.render('../views/message', {
      big_message: "Greška!",
      small_message: "Došlo je do greške, molimo Vas da unesete adresu e-pošte.",
      register_failed: false,
      register_sucess: false
    });
  }
};

const resetPassword = (req, res) => {
  // noinspection DuplicatedCode
  const token = req.cookies.token;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;

  console.log("Token: " + token);

  if (token) {
    const tokenByTokenPromise = ForgotPasswordToken.findByToken(token);

    tokenByTokenPromise.then((forgotPasswordToken) => {
      forgotPasswordToken.getUser().then((user) => {
        if (password === confirmPassword) {
          lozinka = bcrypt.hash(password, 2, (err, hash) => {
            if (err) {
              res.status(400).send(err);
            } else {
              user.update({
                lozinka: hash
              }).then(() => {
                console.log("User updated" + hash);
                forgotPasswordToken.destroy().then(() => {
                  res.render('../views/message', {
                    big_message: "Uspjeh!",
                    small_message: "Uspješno je postavljena nova lozinka, molimo Vas da se ponovno prijavite.",
                    register_failed: false,
                    register_sucess: false
                  });
                }).catch((error) => res.status(400).send(error));
              }).catch((error) => res.status(400).send(error));
            }
          });
        } else {
          res.render('../views/message', {
            big_message: "Greška!",
            small_message: "Došlo je do greške, lozinke nisu jednake, molimo Vas da zatražite novo ponovno postavljanje lozinke.",
            register_failed: false,
            register_sucess: false
          });
        }
      }).catch((error) => res.status(400).send(error));
    }).catch((error) => res.status(400).send(error));
  } else {
    res.render('../views/message', {
      big_message: "Greška!",
      small_message: "Došlo je do greške, molimo Vas da zatražite novo ponovno postavljanje lozinke.",
      register_failed: false,
      register_sucess: false
    });
    res.end();
  }
};

const generateResetPasswordForm = (req, res) => {
  res.cookie('token', req.query.token, {maxAge: 7200000, httpOnly: true});
  res.redirect('/reset-password/form');
};

const createUser = (req, res) => {
  let lozinka = req.body.password;
  lozinka = bcrypt.hash(lozinka, 2, (err, hash) => {
    if (err) console.log(err);
    else {
      //create newUser
      var newUser;
      if (req.body.razina_ovlasti) {
        newUser = new User.User({
          ime: "",
          prezime: "",
          razina_ovlasti: 2,
          korisnicko_ime: req.body.username,
          email: req.body.email,
          lozinka: hash,
          drzava: "",
          dob: 18
        });
        console.log(JSON.stringify(newUser) + " ADMIN");
      } else {
        newUser = new User.User({
          ime: req.body.name,
          prezime: req.body.lastName,
          razina_ovlasti: 3,
          korisnicko_ime: req.body.username,
          email: req.body.email,
          lozinka: hash,
          drzava: req.body.country,
          dob: req.body.age
        });
      }
      // Insert newUser into table
      const findOrCreateUserPromise = User.findOrCreateUser(newUser);

      findOrCreateUserPromise.then(function (result) {
          created = result[1]; // boolean stating if it was created or not

          if (!created) {
            if(req.body.razina_ovlasti){
              res.render('../views/message', {
                big_message: "Neuspješna registracija!",
                small_message: "Korisnik s navedenim e-mailom i korisničkim imenom već postoji!",
                register_failed: false,
                register_sucess: false,
                addAdmin_failed: true

              });
            } else{
              res.render('../views/message', {
                big_message: "Neuspješna registracija!",
                small_message: "Korisnik s navedenim e-mailom i korisničkim imenom već postoji!",
                register_failed: true,
                register_sucess: false,
              });
          }
          } else {

            jwt.sign(
              {
                user: newUser.korisnicko_ime,
              },
              EMAIL_SECRET,
              {
                expiresIn: '1d',
              },
              (err, emailToken) => {
                var url;
                if (req.body.razina_ovlasti) {
                  // vodi ga na stranicu s formom za podatke ako je admin
                  url = SERVER_URL + `/users/adminRegistration/${emailToken}`
                } else {
                  url = SERVER_URL + `/confirmation/${emailToken}`;
                }
                // setup email data with unicode symbols
                let mailOptions;
                if (req.body.razina_ovlasti) {
                  mailOptions = {
                    from: '"AvioMuzej Email Confirmation" <aviomuzej@gmail.com>', // sender address
                    to: newUser.email, // list of receivers
                    subject: 'Aviomuzej Administrator Register Data', // Subject line
                    html: `Your username is ${newUser.korisnicko_ime} and your password is ${req.body.password}.
                          Please click this email to complete your registration: <a href="${url}">${url}</a>`
                  }
                } else {
                  mailOptions = {
                    from: '"AvioMuzej Email Confirmation" <aviomuzej@gmail.com>', // sender address
                    to: newUser.email, // list of receivers
                    subject: 'Aviomuzej Confirm Email', // Subject line
                    html: `Please click this email to confirm your email: <a href="${url}">${url}</a>`
                  }
                }
                transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                      return console.log(error);
                    }
                    console.log('Message sent: %s', info.messageId);
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                  }
                );
                console.log('Sending confirmation email');
              });
            if (req.body.razina_ovlasti) {
              res.redirect('/control_panel');
            } else {
              res.render('../views/message', {
                big_message: "Uspješna registracija!",
                small_message: "Molimo Vas da potvrdite svoju registraciju klikom na link koji vam poslan na vašu e-mail adresu. Nakon toga, možete se prijaviti!",
                register_failed: false,
                register_sucess: true
              });
            }
          }
        }
      ).catch(err => {
        console.log(err);
      });
    }
  });
}

const addAdministrator = (req, res) => {
  var user = {
    body: {
      razina_ovlasti: 2,
      username: req.body.korisnicko_ime,
      email: req.body.email,
      password: req.body.lozinka
    }
  }
  UserInstance.count({where: {
    razina_ovlasti: 2
  }}).then(count => {
    if(count >= 5){
      res.render('../views/message', {
        big_message: "Nemoguće dodati administratora!",
        small_message: "Maksimalan broj administratora (5) dostignut!",
        register_failed: false,
        register_sucess: false,
        login_failed: false
      });
    } else {
      createUser(user, res);
    }
  })
}
const deleteAdministrator = (req, res) => {
  User.deleteUser(req.body.korisnicko_ime);
  res.redirect('/control_panel');
}

const confirmEmail = async (req, res) => {
  const output = jwt.verify(req.params.token, EMAIL_SECRET);
  const confirmUserRegistrationPromise = User.confirmUserRegistration(output.user);
  await confirmUserRegistrationPromise.then(user => console.log(user)).catch(err => res.send(err));
  res.render('../views/message', {
    big_message: "Uspješna potvrda registracije!",
    small_message: "Možete se prijaviti pritiskom na gumb!",
    register_failed: false,
    register_sucess: true
  });
}

const confirmAdmin = async (request, response) => {
  const output = jwt.verify(request.params.token, EMAIL_SECRET);
  const confirmUserRegistrationPromise = User.confirmUserRegistration(output.user);
  await confirmUserRegistrationPromise.then(user => {
    console.log(user);
  }).catch(err => response.send(err));
  request.session.adminName = output.user;
  request.session.username = output.user;
  request.session.isAdmin = true;
  request.session.isOwner = false;
  request.session.loggedin = true;
  let userInfo = {
    username:output.user,
    isAdmin:request.session.isAdmin,
    isOwner:request.session.isOwner
  }
  User.loginUser(output.user).then(user => {
    console.log(output.user + ' logged in!')
  }).catch(err => console.log(err));
  response.cookie('aviomuzejUserName', userInfo, {maxAge: 52*604800000}).render('../views/message', {
    big_message: "Uspješna potvrda registracije!",
    small_message: "Molimo vas da odete na svoju stranicu profila klikom na gumb i popunite svoje podatke!",
    register_failed: false,
    admin_register_sucess: true
  });
}


const updatedInfo = (request, response) => {
  const updatedInfoPromise = User.updateInfo(request.session.username, request.body.ime, request.body.prezime, request.body.dob, request.body.drzava);
  updatedInfoPromise.then((result1) => {

    console.log(result1),
      // response.send('promijenjeno: '+result1);
      response.render('../views/message', {
        big_message: "Podatci uspješno promijenjeni!",
        small_message: " ",
        register_failed: false,
        register_sucess: false,
        login_failed: false
      });
  }).catch(err => {
    console.log(err);
  })
};
const updatedInfoAdmin = (request, response) => {
  const updatedInfoPromise = User.updateInfo(request.body.korisnicko_ime, request.body.ime, request.body.prezime, request.body.dob, request.body.drzava);
  updatedInfoPromise.then((result1) => {

    console.log(result1),
      response.render('../views/message', {
        big_message: "Podatci uspješno promijenjeni!",
        small_message: " ",
        register_failed: false,
        register_sucess: false,
        login_failed: false
      });
  }).catch(err => {
    console.log(err);
  })
};

const deleteUser = (request, response) =>{
  User.deleteUser(request.session.username).then((result) => {

    //logout
    request.session.destroy();
    if(request.cookies.aviomuzejUserName) response.clearCookie('aviomuzejUserName', {maxAge: 52*604800000, path: '/'});

    response.render('../views/message', {
      big_message: "Uspješno obrisan korisnički račun",
      small_message: " ",
      register_failed: false,
      register_sucess: false,
      login_failed: false
    });
  }).catch(err => {
    response.render('../views/message', {
      big_message: "Dogodila se greška!",
      small_message: " ",
      register_failed: false,
      register_sucess: false,
      login_failed: false
    });
  })
};

module.exports = {
  getAllUsers,
  getUserByID,
  getUserByEmail,
  createUser,
  confirmEmail,
  authUser,
  logOutUser,
  debugUser,
  sendResetPasswordEmail,
  resetPassword,
  addAdministrator,
  confirmAdmin,
  updatedInfo,
  updatedInfoAdmin,
  deleteAdministrator,
  generateResetPasswordForm,
  deleteUser
};