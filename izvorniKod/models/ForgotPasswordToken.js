const Sequelize = require('sequelize');
const db = require('../config/database');
const User = require("./User").User;


const ForgotPasswordToken = db.define('forgotPasswordToken',
  {
    sifra_osoba : {
      type: Sequelize.INTEGER,
      primaryKey: true,
      references: "korisnici",
      referencesKey: "sifra_osoba"
    },
    token : {
      type: Sequelize.TEXT,
      primaryKey: true
    },
    istek : {
      type: Sequelize.DATE
    },
  }, {
    tableName : 'zaboravljena_lozinka_tokeni',
    timestamps : false
  });

ForgotPasswordToken.belongsTo(User, {
  foreignKey: 'sifra_osoba'
});

const createNewToken = (sifra,token,istek) => {
  return ForgotPasswordToken.create({
    sifra_osoba: sifra,
    token: token,
    istek: istek
  })
};


const findByToken = (token) => {
  return ForgotPasswordToken.findOne({
    where:{
      token: token
    }
  });
};


module.exports = {
  ForgotPasswordToken,
  createNewToken,
  findByToken
};