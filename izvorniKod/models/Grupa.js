const Sequelize= require('sequelize');
const db= require('../config/database');

const Grupa=db.define('grupa',{
    sifra_grupa:{
    type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    naziv_grupa:{
        type: Sequelize.STRING
    }
},{
    tableName : 'grupa_izlozaka',
    timestamps : false
});

const getAllExhibition=()=> {
    return Grupa.findAll({raw: true});
}
const getGrupaByNaziv = (naziv) => {
    return Grupa.findAll({
        where: {
            naziv_grupa: naziv
        },
        raw: true
    });
}
const removeGroupById = (id) => {
    return Grupa.destroy({
        where: {
            sifra_grupa: id
        }
    })
};



module.exports={
    Grupa,
    getAllExhibition,
    getGrupaByNaziv,
    removeGroupById
}