const Sequelize= require('sequelize');
const db= require('../config/database');
const IzlozakData = require('../models/IzlozakData').IzlozakData;
const Visits = require('../models/Visits').Visits;
const Plays = require('../models/Plays').Plays;
const Proizvodac = require('../models/Proizvodac').Proizvodac;
const Motor = require('../models/Motor').Motor;

const Izlozak=db.define('izlozak', {
    sifra_izlozak: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    sifra_grupa: {
        type: Sequelize.INTEGER,
    },
    sifra_proizvodac: {
        type: Sequelize.INTEGER
    },
    naziv_avion: {
        type: Sequelize.STRING
    },

    najveca_brzina: {
         type: Sequelize.FLOAT
    }, 

    raspon_krila: {
        type: Sequelize.FLOAT
    },

    godina_proizvodnje: {
        type: Sequelize.DATE
    },
    broj_izgradenih: {
        type: Sequelize.INTEGER
    },
    sifra_motor: {
        type: Sequelize.INTEGER
    }
}, {
    tableName : 'izlozak',
    timestamps : false
});

Izlozak.hasOne(IzlozakData, {as: 'izlozak_data',foreignKey: 'sifra_izlozak'});
Izlozak.hasOne(Visits, {as: 'posjecenost', foreignKey: 'sifra_izlozak'});
Izlozak.hasOne(Plays, {as: 'reprodukcija', foreignKey: 'sifra_izlozak'});
Izlozak.belongsTo(Motor,{as:'motor',foreignKey: 'sifra_motor'});
Izlozak.belongsTo(Proizvodac, {as: 'proizvodac', foreignKey: 'sifra_proizvodac'});
IzlozakData.belongsTo(Izlozak, {as: 'izlozak', foreignKey: 'sifra_izlozak'});

const getAllExhibits=()=> {
    return Izlozak.findAll({include: ['izlozak_data', 'posjecenost','proizvodac','motor', 'reprodukcija'], raw: true});
}

const getExhibitByID = (PK) => {
    return Izlozak.findByPk(PK, {plain: true});
}
const getExhibitByGroupID = (groupID) => {
    return Izlozak.findAll({
        where: {
          sifra_grupa: groupID
        }, 
        include: ['izlozak_data', 'posjecenost','proizvodac','motor'],
        raw:true
      })
}
// fk constraint ce maknut povezano iz drugih tablica
const removeExhibitByName = (naziv_avion) => {
    return Izlozak.destroy({
        where: {
            naziv_avion: naziv_avion
        }
    })
}
const removeExhibitById = (id) => {
    return Izlozak.destroy({
        where: {
            sifra_izlozak: id
        }
    })
}
module.exports= {
    Izlozak,
    getAllExhibits,
    getExhibitByID,
    getExhibitByGroupID,
    removeExhibitById,
    removeExhibitByName
}