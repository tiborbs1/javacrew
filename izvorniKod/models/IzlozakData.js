const Sequelize= require('sequelize');
const db= require('../config/database');
const Proizvodac = require('../models/Proizvodac').Proizvodac;
const Motor = require('../models/Motor').Motor;
const Grupa = require('../models/Grupa').Grupa;
const Visits = require('../models/Visits').Visits;
const Plays = require('../models/Plays').Plays;

var noviIzlozak = {
    sifra_izlozak: 0,
    audio_pointer: "",
    slika_pointer: "",
    izlozak_opis: "",
    izlozak: {
        sifra_izlozak: 0,
        sifra_grupa: 0,
        sifra_proizvodac: 0,
        naziv_avion: "",
        najveca_brzina: 0,
        raspon_krila: 0,
        godina_proizvodnje: "",
        broj_izgradenih: 0,
        sifra_motor: 0
    }
}

const IzlozakData=db.define('izlozak_data', {
    sifra_izlozak: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        references: "izlozak",
        referencesKey: "sifra_izlozak"
    },
    audio_pointer: {
        type: Sequelize.STRING
    },
    slika_pointer: {
        type: Sequelize.STRING
    },
    izlozak_opis: {
        type: Sequelize.STRING
    }
}, {
    tableName : 'izlozak_data',
    timestamps : false
});


const getExhibitDataByID = (PK) => {
    return IzlozakData.findByPk(PK, {plain: true});
}


const createIzlozak = (newIzlozakData) => {
    noviIzlozak.audio_pointer = newIzlozakData.audio_pointer;
    noviIzlozak.slika_pointer = newIzlozakData.slika_pointer;
    noviIzlozak.izlozak_opis = newIzlozakData.izlozak_opis;
    noviIzlozak.izlozak.naziv_avion = newIzlozakData.naziv_avion;
    noviIzlozak.izlozak.najveca_brzina =  newIzlozakData.najveca_brzina;
    noviIzlozak.izlozak.raspon_krila = newIzlozakData.raspon_krila;
    noviIzlozak.izlozak.godina_proizvodnje = newIzlozakData.godina_proizvodnje;
    noviIzlozak.izlozak.broj_izgradenih = newIzlozakData.broj_izgradenih;
    


        Grupa.findOrCreate({
        where: {naziv_grupa: newIzlozakData.naziv_grupa},
        defaults: {naziv_grupa: newIzlozakData.naziv_grupa}
        })
        .then(([rezultat, created]) => {
            const rez = rezultat.get({plain: true});
            noviIzlozak.izlozak.sifra_grupa = rez.sifra_grupa;
        }).then(() => {
           return Proizvodac.findOrCreate({
                where: {naziv_proizvodac: newIzlozakData.naziv_proizvodac},
                defaults: {naziv_proizvodac: newIzlozakData.naziv_proizvodac}
            }).then(([rezultat, created]) => {
                const rez = rezultat.get({plain: true});
                noviIzlozak.izlozak.sifra_proizvodac = rez.sifra_proizvodac;
            }).catch(e => console.log(e));
        }).then(() => {
            return Motor.findOrCreate({
                where: {naziv_motor: newIzlozakData.naziv_motor},
                defaults: {naziv_motor: newIzlozakData.naziv_motor}
            }).then(([rezultat, created]) => {
                const rez = rezultat.get({plain: true});
                noviIzlozak.izlozak.sifra_motor = rez.sifra_motor;
            }).catch(e => console.log(e));
        }).then(() => {
            return IzlozakData.max('sifra_izlozak').
                then(max => {
                    noviIzlozak.sifra_izlozak = max + 1;
                    noviIzlozak.izlozak.sifra_izlozak = max + 1;
                }).catch(e => console.log(e));

        }).then(() => {
            return IzlozakData.create(noviIzlozak, 
                {
                    include: [ 'izlozak' ]
                }).catch(e => console.log(e));
        }).then(() => {
            var posjecenost = {
                sifra_izlozak: noviIzlozak.sifra_izlozak,
                broj_posjeta: 0,
                vrijeme: '00:00:00'
            }
            return Visits.create(posjecenost)
                    .catch(e => console.log(e));
        }).then(() => {
            var reprodukcija = {
                sifra_izlozak: noviIzlozak.sifra_izlozak,
                broj_reprodukcija: 0
            }
            Plays.create(reprodukcija).catch(e => console.log(e));
        });
        
}

module.exports = {
    IzlozakData,
    getExhibitDataByID,
    createIzlozak
}