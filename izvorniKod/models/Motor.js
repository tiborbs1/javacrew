const Sequelize= require('sequelize');
const db= require('../config/database');
const Izlozak=require('../models/Izlozak').Izlozak;


const Motor=db.define('motor', {
    sifra_motor: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    naziv_motor: {
        type: Sequelize.STRING
    }
}, {
    tableName : 'motori',
    timestamps : false
});

module.exports={
    Motor
}