const Sequelize= require('sequelize');
const db= require('../config/database');

const Plays=db.define('reprodukcija', {
    sifra_izlozak: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: "izlozak",
        referencesKey: "sifra_izlozak"
    },
    broj_reprodukcija: {
        type: Sequelize.INTEGER
    }
}, {
    tableName : 'reprodukcija',
    timestamps : false
});

const getAllPlays = () => {
    return Plays.findAll({
        include:['izlozak'],
        raw: true
    });
}

const increasePlays = (sifra) => {
    return Plays.update({
        broj_reprodukcija : Sequelize.literal('broj_reprodukcija + 1')
    },{
        where: {sifra_izlozak:sifra}
    });
}

module.exports = {
    Plays,
    getAllPlays,
    increasePlays
}