const Sequelize= require('sequelize');
const db= require('../config/database');
const Izlozak=require('../models/Izlozak').Izlozak;

const Proizvodac=db.define('proizvodac', {
    sifra_proizvodac: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    naziv_proizvodac: {
        type: Sequelize.STRING
    }
}, {
    tableName : 'proizvodaci',
    timestamps : false
});


const getManufacturer=()=> {
    return Proizvodac.findAll({
            include: ['izlozak'],
            raw:true
    })
} 

module.exports={
    Proizvodac,
    getManufacturer
}