const Sequelize= require('sequelize');
const db= require('../config/database');

const Promo=db.define('promo',{
    sifra_materijal:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    materijal_pointer:{
        type: Sequelize.STRING
    },
    naziv_promo:{
        type:Sequelize.STRING
    }
},{
    tableName : 'promo_materijali',
    timestamps : false
});

const getAllPromos=()=> {
    return Promo.findAll({raw: true});
}

const getPromoByID = (PK) => {
    return Promo.findByPk(PK, {raw: true});
}

module.exports={
    Promo,
    getAllPromos,
    getPromoByID
}