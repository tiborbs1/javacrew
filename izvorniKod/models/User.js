const Sequelize = require('sequelize');
const db = require('../config/database')

const Op = Sequelize.Op;

const User = db.define('user',
{
    sifra_osoba : {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    ime : {
        type: Sequelize.STRING
    },
    prezime : {
        type: Sequelize.STRING
    },
    korisnicko_ime : {
        type: Sequelize.STRING,
    },
    email : {
        type: Sequelize.STRING,
    },
    drzava : {
        type: Sequelize.STRING
    },
    razina_ovlasti : {
        type: Sequelize.TINYINT,
    },
    dob : {
        type: Sequelize.SMALLINT
    },
    lozinka : {
        type: Sequelize.STRING,
    },
    potvrda : {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    aktivan : {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
}, {
    tableName : 'korisnici',
    timestamps : false
});

const getAllUsers = () => {
    return User.findAll();
}
const getAllAdmins = () => {
    return User.findAll({
        where: {
            razina_ovlasti: 2,
            potvrda: true
        }
    });
}

const loginUser = (username) => {
    return User.update({ aktivan: true }, { where: { korisnicko_ime:username } });
}


const logOutUser = (username) => {
    return User.update({ aktivan: false }, { where: { korisnicko_ime:username } });
}

const findUserByID = (id)=> {
    return User.findByPk(id);
}

const findUserByEmail = (email) => {
    return User.findOne({
          where:{
              email: email
            }
        });
}

const findRegisteredUserByUserName = (username) => {
    return User.findOne({
        where: {
          korisnicko_ime: username,
          potvrda: true
        }
    });
}

const findOrCreateUser = (newUser) => {
    return User.findOrCreate({
        where: {
          [Op.or]: [{email: newUser.email}, {korisnicko_ime: newUser.korisnicko_ime}]
        },
        defaults:{
          ime: newUser.ime,
          prezime: newUser.prezime,
          razina_ovlasti: newUser.razina_ovlasti,
          korisnicko_ime: newUser.korisnicko_ime,
          email: newUser.email,
          drzava: newUser.drzava,
          dob: newUser.dob,
          lozinka: newUser.lozinka
        }
      });
}

const confirmUserRegistration = (username)=> {
    return User.update({ potvrda: true }, { where: { korisnicko_ime:username } });
};

const isAdmin = (username) => {
  return User.findOne({
        where: {
          korisnicko_ime: username,
          potvrda: true
        }, raw:true
    }).then(user => {
        razina_korisnik = user.razina_ovlasti;
        if(razina_korisnik===2){
            return true
        } else return false;
    }).catch(err=>console.log(err)); 
}

const isOwner = (username) => {
    return User.findOne({
        where: {
          korisnicko_ime: username,
          potvrda: true
        }, raw:true
    }).then(user => {
        razina_korisnik = user.razina_ovlasti;
        if(razina_korisnik===1){
            return true
        } else return false;
    }).catch(err=>console.log(err)); 
}

const countActiveUsers = () => {
    return User.count({
    where: {
        razina_ovlasti:3,
        aktivan:true,
        potvrda: true
    }, raw:true
});
}

const countActiveAdmins = ()=>{
    return User.count({
        where: {
            razina_ovlasti:2,
            aktivan:true,
            potvrda: true
        }, raw:true
    });
}

const getActiveAdmins = ()=> {
    return User.findAll({
        where: {
            razina_ovlasti:2,
            aktivan:true,
            potvrda:true
        }, raw: true
    });
}
const updateInfo=(trenutno_ime,novo_ime,novo_prezime,nova_dob,nova_drzava)=> {
    return User.update(
        {
            ime: novo_ime,
            prezime: novo_prezime,
            dob: nova_dob,
            drzava: nova_drzava
        }, {
            where: { korisnicko_ime: trenutno_ime}
        })
}

const deleteUser = (username) => {
    return User.destroy({
        where: { korisnicko_ime: username}
    })
}



module.exports = {
    User,
    getAllUsers,
    findUserByID,
    findUserByEmail,
    findRegisteredUserByUserName,
    findOrCreateUser,
    confirmUserRegistration, 
    isAdmin,
    isOwner,
    getAllAdmins,
    getActiveAdmins,
    countActiveAdmins,
    loginUser,
    logOutUser,
    countActiveUsers,
    updateInfo,
    deleteUser
};