const Sequelize= require('sequelize');
const db= require('../config/database');

const Visits=db.define('posjecenost', {
    sifra_izlozak: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        references: "izlozak",
        referencesKey: "sifra_izlozak"
    },
    broj_posjeta: {
        type: Sequelize.INTEGER
    },
    vrijeme: {
        type: Sequelize.TIME
    }
}, {
    tableName : 'posjecenost',
    timestamps : false
});

const getAllVisits = ()=> {
    return Visits.findAll({raw:true});
}

const increaseVisits = (sifra) => {
    return Visits.update({
        broj_posjeta : Sequelize.literal('broj_posjeta + 1')
    },{
        where: {sifra_izlozak:sifra}
    });
}

module.exports = {
    Visits,
    getAllVisits,
    increaseVisits
}