// Wait for the DOM to be ready
$(function () {

    jQuery.validator.setDefaults({
      highlight: function (element) {
        jQuery(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function (element) {
        jQuery(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
      },
      errorElement: 'div',
      errorClass: 'error',
      errorPlacement: function (error, element) {
        console.log(error);
        // Add the `invalid-feedback` class to the error element
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      }
    });
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='adminForm']").validate({
      // Specify validation rules
      rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        email: {
            required: true,
            email: true
        },
        korisnicko_ime: {
          required: true,
          pattern: true,
          minlength: 5
        },
  
        lozinka: {
          required: true,
          minlength: 8
        }
      },
      // Specify validation error messages
      messages: {
        email: {
            required: 'Morate zadati e-mail admina!',
            email: 'E-mail nije ispravnog oblika!'
        },
        korisnicko_ime: {
          required: 'Morate napisati korisnicko ime admina!',
          minlength: 'Korisničko ime mora imati najmanje pet znakova!'
        },

        lozinka: {
          required: 'Morate napisati lozinku za admina',
          minlength: 'Lozinka mora imati najmanje 8 znakova'
        }
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function (form) {
        form.submit();
      }
    });

    $.validator.addMethod("pattern",
    function(value, element) {
        return value.match((/^[a-zA-Z]+[a-zA-Z_0-9]*$/));
    }, 'Korisničko ime mora započeti slovom!');
  });

