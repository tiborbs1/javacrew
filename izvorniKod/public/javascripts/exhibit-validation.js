// Wait for the DOM to be ready
$(function () {

    jQuery.validator.setDefaults({
      highlight: function (element) {
        jQuery(element).closest('.form-control').addClass('is-invalid');
      },
      unhighlight: function (element) {
        jQuery(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
      },
      errorElement: 'div',
      errorClass: 'error',
      errorPlacement: function (error, element) {
        console.log(error);
        // Add the `invalid-feedback` class to the error element
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      }
    });
    // Initialize form validation on the registration form.
    // It has the name attribute "registration"
    $("form[name='exhibitForm']").validate({
      // Specify validation rules
      rules: {
        // The key name on the left side is the name attribute
        // of an input field. Validation rules are defined
        // on the right side
        najveca_brzina: {
            required: true,
            number: true
        },
        raspon_krila: {
          required: true,
          number: true
        },
  
        broj_izgradenih: {
          required: true,
          number: true
        },
        godina_proizvodnje: {
            required: true,
            dateFormat: true
        },
        audio: {
            required: true
        },
        image: {
            required: true
        },
        OpisIzloska: {
            required: true
        },
        proizvodac: {
            required: true
        },
        naziv: {
            required: true
        },
        motor: {
            required: true
        }
      },
      // Specify validation error messages
      messages: {
       najveca_brzina: {
          required: "Unesite najveću brzinu!",
          number: "Najveća brzina mora biti broj!"
        },
        raspon_krila: {
            required: "Unesite raspon krila!",
            number: "Raspon krila mora biti broj!"
          },
        broj_izgradenih: {
            required: "Unesite broj izgrađenih!",
            number: "Broj izgrađenih mora biti broj!"
          },
        godina_proizvodnje: {
            required: "Unesite godinu proizvodnje!"
          },
          audio: {
            required: "Unesite audio zapis!"
        },
        image: {
            required: "Unesite sliku!"
        },
        OpisIzloska: {
            required: "Unesite opis izloška!"
        },
        proizvodac: {
            required: "Unesite naziv proizvođača!"
        },
        naziv: {
            required: "Unesite naziv zrakoplova!"
        },
        motor: {
            required: "Unesite naziv motora!"
        }
     
      },
      // Make sure the form is submitted to the destination defined
      // in the "action" attribute of the form when valid
      submitHandler: function (form) {
        form.submit();
      }
    });

    $.validator.addMethod("dateFormat",
    function(value, element) {
        return value.match((/^(\d{4})\-(\d{1,2})\-(\d{1,2})$/));
    },
    "Unesite godinu proizvodnje u formatu yyyy-mm-dd.");
  });

