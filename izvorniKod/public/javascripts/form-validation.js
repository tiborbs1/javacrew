// Wait for the DOM to be ready
$(function() {

  jQuery.validator.setDefaults({
    highlight: function(element) {
        jQuery(element).closest('.form-control').addClass('is-invalid');
    },
    unhighlight: function(element) {
        jQuery(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
    },
    errorElement: 'div',
    errorClass: 'error',
    errorPlacement: function ( error, element ) {
      // Add the `invalid-feedback` class to the error element
      error.addClass( "invalid-feedback" );
      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.next( "label" ) );
      } else {
        error.insertAfter( element );
      }
    }
  });
  jQuery.validator.addMethod(
    "regex",
    function (value, element, regexp) {
      var re = new RegExp(regexp);
      return this.optional(element) || re.test(value);
    },
    "Please check your input."
  );

  // Initialize form validation on the registration form.
  // It has the name attribute "registration"

  const name_regex = /^(?:[A-Za-zčćžšČĆŠĐŽ]+(?: *|-)?)+$/;
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='signupForm']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 5
      },
      username: {
        required: true,
        minlength: 5
      },
      confirmPassword: {
        required: true,
        minlength: 5,
        equalTo: "input[name='password']"
      },
      name: {
        required: true,
        regex: name_regex
      },
      lastName: {
        required: true,
        regex: name_regex
      },
      country: {
        required: true,
        regex: /^[A-Za-zčćšđžČĆŠĐŽ ]+$/
      },
      age: {
        required: true
      }
    },
    // Specify validation error messages
    messages: {
      username: {
        required:"Unesite korisničko ime",
        minlength: "Korisničko ime mora imati barem 5 znakova"
      },
      password: {
        required: "Unesite lozinku",
        minlength: "Lozinka mora imati barem 8 znakova"
      },
      email: "Unesite valjanu e-mail adresu",
      confirmPassword: {
        required: "Potvrdite lozinku",
        equalTo: "Lozinke se ne podudaraju" 
      },
      name: {
        required: "Unesite ime",
        regex: 'Unesite važeće ime'
      },
      lastName: {
        required: "Unesite prezime",
        regex: 'Unesite važeće prezime'
      },
      country: {
        required: "Unesite državu",
        regex: 'Unesite važeću državu'
      },
      age: {
        required: "Unesite svoju dob"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});