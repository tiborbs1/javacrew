// Wait for the DOM to be ready
$(function () {

  jQuery.validator.setDefaults({
    highlight: function (element) {
      jQuery(element).closest('.form-control').addClass('is-invalid');
    },
    unhighlight: function (element) {
      jQuery(element).closest('.form-control').removeClass('is-invalid').addClass('is-valid');
    },
    errorElement: 'div',
    errorClass: 'error',
    errorPlacement: function (error, element) {
      console.log(error);
      // Add the `invalid-feedback` class to the error element
      error.addClass("invalid-feedback");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.next("label"));
      } else {
        error.insertAfter(element);
      }
    }
  });

  jQuery.validator.addMethod(
    "regex",
    function (value, element, regexp) {
      var re = new RegExp(regexp);
      return this.optional(element) || re.test(value);
    },
    "Please check your input."
  );

  // Initialize form validation on the registration form.
  // It has the name attribute "registration"

  const name_regex = /^(?:[A-Za-zčćšđžČĆŠĐŽ]+(?: *|-)?)+$/;

  $("form[name='profileForm']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      email: {
        required: true,
        email: true
      },
      korisnicko_ime: {
        required: true,
        minlength: 5
      },
      ime: {
        required: true,
        regex: name_regex
      },
      prezime: {
        required: true,
        regex: name_regex
      },
      drzava: {
        required: true,
        regex: /^[A-Za-zčćšđžČĆŠĐŽ ]+$/
      },
      dob: {
        required: true,
        digits: true,
        min: 1,
        max: 130
      }
    },
    // Specify validation error messages
    messages: {
      korisnicko_ime: {
        required: "Unesite korisničko ime",
        minlength: "Korisničko ime mora imati barem 5 znakova"
      },
      email: "Unesite valjanu e-mail adresu",

      ime: {
        required: "Unesite ime",
        regex: "Unesite valjano ime"
      },
      prezime: {
        required: "Unesite prezime",
        regex: "Unesite valjano prezime"
      },
      drzava: {
        required: "Unesite državu",
        regex: "Unesite valjano ime države"
      },
      dob: {
        required: "Unesite svoju dob",
        digits: "Unesite valjanu dob",
        min: "Minimalna valjana dob je 1",
        max: "Maksimalna valjana dob je 130"

      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function (form) {
      form.submit();
    }
  });
});