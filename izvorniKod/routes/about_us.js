var express = require('express');
var router = express.Router();
const Grupa = require('../models/Grupa');
let aboutVisits = 0;

const checkCookie = require('../config/cookie');
router.use(checkCookie);

/* GET about page. */
router.get('/', (req,res) => {
  aboutVisits=aboutVisits+1;
  console.log(aboutVisits);
  const getAllExhibitionsPromise = Grupa.getAllExhibition();
  getAllExhibitionsPromise.then((grupa) => {
    res.render('../views/about', {
      grupa,
      session_isAdmin: req.session.isAdmin,
      session_active: req.session.loggedin,
      session_isOwner: req.session.isOwner
    });
  }).catch(err=>res.send(err));
});
module.exports = {
  router,
  aboutVisits
}