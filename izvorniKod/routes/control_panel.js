var express = require('express');
var router = express.Router();
const Grupa = require('../models/Grupa');
const grupaController = require('../controllers/grupaController');
const izlozakDataController = require('../controllers/izlozakDataController');
const userController = require('../controllers/userController');
const izlozakController = require('../controllers/izlozakController');
const User = require('../models/User');
const Izlozak = require('../models/Izlozak')

const checkCookie = require('../config/cookie');
router.use(checkCookie);


/* GET control panel page. */
router.get('/', function(req, res, next) {
  if((req.session.isAdmin || req.session.isOwner) && req.session.loggedin) {
  const getAllExhibitionsPromise = Grupa.getAllExhibition();
  const getAllAdminsPromise = User.getAllAdmins();
  const getAllActiveAdminsPromise = User.getActiveAdmins();
  const getActiveUserCountPromise = User.countActiveUsers();
  const countActiveAdminsPromise = User.countActiveAdmins();
  const getAllExhibitsPromise = Izlozak.getAllExhibits();
  var posjecenost = require('../routes/index').getIndexVisits();
  console.log('Posjecenost iz control panela je: ' + posjecenost);

  Promise.all([getAllExhibitionsPromise, getAllAdminsPromise, getAllActiveAdminsPromise, getActiveUserCountPromise, countActiveAdminsPromise, getAllExhibitsPromise]).then((rezultati) => {
    console.log(posjecenost)
    res.render('../views/control_panel', {
      grupa: rezultati[0],
      admin: rezultati[1],
      aktivni_admini: rezultati[2],
      broj_korisnika: rezultati[3],
      broj_admina: rezultati[4],
      session_isAdmin:req.session.isAdmin,
      session_isOwner: req.session.isOwner,
      session_active:req.session.loggedin,
      izlozak: rezultati[5],
      posjecenost_glavne:posjecenost
    });
  }).catch(err=>res.send(err)); 
} else {
  res.redirect("/");
}
});

router.post('/addExhibit', (req, res) => {
  var imageFile = req.files.image;
  var audioFile = req.files.audio;

  imageFile.mv('./public/images/' + imageFile.name, function(error){
    if(error){
      console.log(error);
    } else {
      console.log("Uspješno dodano");
    }
  });
  audioFile.mv('./public/audio/' + audioFile.name, function(error){
    if(error){
      console.log(error);
    } else {
      console.log("Uspješno dodano");
    }
  });
  
// Obrada u bazi
  izlozakDataController.createIzlozakData(req, res);
  res.redirect("/control_panel");
});

router.post('/deleteExhibitGroup', grupaController.deleteGrupa);

router.post('/addAdmin', userController.addAdministrator);
router.post('/deleteAdmin', userController.deleteAdministrator);

router.post('/deleteIzlozak', izlozakController.deleteIzlozak);



module.exports = router;