var express = require('express');
var router = express.Router();
const Promo = require('../models/Promo');


router.get('/:id', (req,res)=> {
    const getPromoByIDPromise = Promo.getPromoByID(req.params.id);

    getPromoByIDPromise.then(promo => {
        console.log(promo.materijal_pointer);
        res.download(promo.materijal_pointer, (err)=>{
            console.log(err);
        })
    })
});

module.exports = router;