const express = require('express');
const router = express.Router();
const izlozakDataController = require('../controllers/izlozakDataController');

const checkCookie = require('../config/cookie');
router.use(checkCookie);

router.get('/:id', izlozakDataController.getSingleExhibitData);
module.exports = router;