const express = require('express');
const router = express.Router();
const izlozakController = require('../controllers/izlozakController');

const checkCookie = require('../config/cookie');
router.use(checkCookie);

//get exhibits page
router.get('/', izlozakController.getAllExhibits);
router.get('/:id',izlozakController.getAllExhibitsByGroupId);

module.exports = router;