var express = require('express');
var router = express.Router();
const Grupa = require('../models/Grupa');
let indexVisits = 0;

const checkCookie = require('../config/cookie');
router.use(checkCookie);

/* GET index page. */
router.get('/', (req,res) => {
  indexVisits=indexVisits+1;
  console.log(indexVisits);
  const getAllExhibitionsPromise = Grupa.getAllExhibition();
  getAllExhibitionsPromise.then((grupa) => {
    res.render('../views/index', {
      grupa,
      session_isAdmin:req.session.isAdmin,
      session_active:req.session.loggedin,
      session_isOwner:req.session.isOwner
    });
  })
  .catch(err => console.log(err));
  });

const getIndexVisits = ()=>{
  return indexVisits;
}
module.exports = {
  router,
  getIndexVisits
}
 
