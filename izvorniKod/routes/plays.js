var express = require('express');
var router = express.Router();
var Plays = require('../models/Plays')

router.get('/increasePlays/:id', function(req, res){
    const sifraToUpdate = req.params.id
    Plays.increasePlays(sifraToUpdate).then((result)=>{
        res.sendStatus(200)
    }).catch(err=>console.log(err));
})

module.exports = router;