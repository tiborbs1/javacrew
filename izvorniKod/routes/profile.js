var express = require('express');
var router = express.Router();
const Grupa = require('../models/Grupa');
const userController = require('../controllers/userController');
const User=require('../models/User');

const checkCookie = require('../config/cookie');
router.use(checkCookie);

router.get('/',(request,response)=>{
    if(request.session.loggedin == false || request.session.loggedin == undefined){
      response.redirect('/');
      response.end();
    }
    let findRegisteredUserByUserNamePromise;
    if(request.session.adminName){
      findRegisteredUserByUserNamePromise = User.findRegisteredUserByUserName(request.session.adminName);
    } else {
     findRegisteredUserByUserNamePromise=User.findRegisteredUserByUserName(request.session.username);
    }
    const getAllExhibitionsPromise=Grupa.getAllExhibition();
    Promise.all([findRegisteredUserByUserNamePromise,getAllExhibitionsPromise]).then((rezultati)=>{
            const kor1= {
              ime: (rezultati[0].get('ime') ? rezultati[0].get('ime') : 'Ime'),
              prezime: (rezultati[0].get('prezime') ? rezultati[0].get('prezime') : 'Prezime'),
              korisnicko_ime: rezultati[0].get('korisnicko_ime'),
              e_mail: rezultati[0].get('email'),
              drzava: (rezultati[0].get('drzava') ? rezultati[0].get('drzava') : 'Država'),
              dob: rezultati[0].get('dob')
        
            }
            console.log(kor1);
            response.render('../views/profile',{
              kor1,
              grupa:rezultati[1],
              session_active: (request.session.adminName ? false : request.session.loggedin),
              session_isAdmin: (request.session.adminName ? true : request.session.isAdmin),
              session_isOwner: (request.session.adminName ? false : request.session.isOwner)
            });
          }).catch(err=>response.send(err));
        });

router.post('/changedInfo',userController.updatedInfo);

module.exports = router;