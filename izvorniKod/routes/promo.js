var express = require('express');
var router = express.Router();
const promoController = require('../controllers/promoController');

const checkCookie = require('../config/cookie');
router.use(checkCookie);
/* GET promo page. */
router.get('/', promoController.getAllPromos);

module.exports = router;
