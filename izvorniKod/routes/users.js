const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');


router.post('/add', userController.createUser);
router.get('/', userController.getAllUsers);
router.post('/auth', userController.authUser);
router.get('/logout', userController.logOutUser);
router.get('/debug', userController.debugUser);
router.post('/sendResetPasswordEmail', userController.sendResetPasswordEmail);
router.get('/resetPassword', userController.generateResetPasswordForm);
router.post('/resetUserPassword', userController.resetPassword);
router.get('/delete', userController.deleteUser);
router.get('/:id', userController.getUserByID);
router.get('/adminRegistration/:token', userController.confirmAdmin);

module.exports = router;