var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

const User = require('../models/User');
const Grupa = require('../models/Grupa');
const Izlozak = require('../models/Izlozak');
const Visits = require('../models/Visits');


chai.use(chaiAsPromised).should();


describe('isAdminPromise', function () {
    it('should return true if user id is of the admin', function () {
        return User.isAdmin('TihanaJK').should.eventually.be.true;
    });

    it('should return undefined if user id is not of the admin', function () {
        return User.isAdmin('tjkormo').should.eventually.be.undefined;
    });
});

describe('getAllExhibitionsPromise', function () {
    let exhibitions = [{
        sifra_grupa: 2,
        naziv_grupa: 'Borbeni zrakoplov'
    }, {
        sifra_grupa: 1,
        naziv_grupa: 'Putnički zrakoplov'
    }, {
        sifra_grupa: 3,
        naziv_grupa: 'Teretni zrakoplov'
    },]


    it('should return all exhibitions', function () {
        return Grupa.getAllExhibition().should.eventually.be.a('array').to.eventually.have.deep.members(exhibitions);
    });
});

describe('exhibitById', function () {
    it('should return exhibit with id', function () {
        return Izlozak.getExhibitByID(1).should.eventually.be.an('object');
    });
    it('should return null if no exhibit with id', function(){
        return Izlozak.getExhibitByID(53).should.eventually.be.equal(null);
    });
});

describe('increaseVisits', function () {
    it('should increase', function () {
        return Visits.increaseVisits(1).increases;
    });
    it('should not increase if id does not exist', function(){
        return Visits.increaseVisits(53).doesNotIncrease;
    })
});

describe('countActiveUsersAndAdmins', function () {
    it('should return correct number of active users', function () {
        return User.countActiveAdmins().should.eventually.be.equal(2);
    });
    it('should return correct number of active admins', function(){
        return User.countActiveUsers().should.eventually.be.equal(5);
    })
});

describe('exhibitsByGroupID', function () {
  it('should return all exhibits which have the groupID', function(){
      return Izlozak.getExhibitByGroupID(2).should.eventually.be.an('array').to.have.length(2);
  });

  it('should return empty array when the groupID does not exist', function(){
    return Izlozak.getExhibitByGroupID(45).should.eventually.be.an('array').to.have.length(0);
});

});
